package com.converter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * @author chakrs
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class WordConverterTest {
    @Spy
	private WordConverter wordConverter;
    
    @Test
	public void testConvertNumber()  {
		String result1 = wordConverter.convertNumber(10);
		assertEquals("ten",result1);
		String result2 = wordConverter.convertNumber(-10);
		assertEquals("minusten",result2);
	}

}
