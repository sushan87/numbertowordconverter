package com.converter;

/**
 * @author chakrs
 *
 */
public class WordConverter {

	private  String[] numNames = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
			"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
			"nineteen" };
	private  String[] specialNames = { "", "thousand", "million", "billion", "trillion", "quadriiliion",
			"quintiliion" };
	private  String[] tenNames = { "", "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty",
			"ninety" };
	/**
	 * @param number
	 * @return
	 */
	private  String convertLessThanThousand(int number) {
		String word = null;
		if (number % 100 < 20) {
			word = numNames[number / 100];
			if (number >= 100) {
				word += "hundred";
			}
			number = number % 100;
		} else {
			word = numNames[number / 100] + " hundred ";
			number = number % 100;
			word += tenNames[number / 10];
			number = number % 10;
		}
		if (number == 0) {
			return word;
		}
		return word + numNames[number];
	}

	/**
	 * @param number
	 * @return
	 */
	public String convertNumber(int number) {
		String prefix = "";
		String word = "";
		if (number == 0) {
			return "zero";
		}
		if (number < 0) {
			number = -number;
			prefix = "minus";
		}
		int place = 0;
		String current = "";
		do {

			int n = number % 1000;

			if (n != 0) {
				word = convertLessThanThousand(n);
				current = word + specialNames[place] + current;
			}

			number = number / 1000;
			place++;
		} while (number > 0);
		return (prefix+current).trim();
	}

	/*
	 * public static void main(String args[]) {
	 * System.out.println(convertGreaterThanThousand(2000001)); }
	 */
}
